<?php

function autoloader($class_name) {
    #List all the class directories in the array
    $array_path = array(
        '/models/',
        '/components/'
    );

    foreach ($array_path as $path) {
        $path = ROOT . $path . $class_name . '.php';
        if(file_exists($path)) {
            include_once $path;
        }
    }
}

spl_autoload_register('autoloader');
